import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://eklipse.gg/')

WebUI.click(findTestObject('Page_Eklipse - Convert Twitch Clip To TikTok, Reels,  Shorts For Free/a_Sign Up For Free'))

WebUI.setText(findTestObject('Page_Eklipse_Register/input_OR_name'), 'FSFORTEST3')

WebUI.setText(findTestObject('Page_Eklipse_Register/input_OR_email'), 'fsfortestacc3@gmail.com')

WebUI.setText(findTestObject('Page_Eklipse_Register/input_OR_password'), '12345Qwe')

WebUI.setText(findTestObject('Page_Eklipse_Register/input_OR_password_confirmation'), '12345Qwe')

WebUI.click(findTestObject('Page_Eklipse_Register/button_Sign Up with Email'))

