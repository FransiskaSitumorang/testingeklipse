import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Page_Eklipse/a_Home'))

WebUI.click(findTestObject('Page_Eklipse/button_Convert to TikTok  Shorts  Reels'))

WebUI.uploadFile(findTestObject('Page_Eklipse studio/button_Upload Clip'), vid)

WebUI.click(findTestObject('Page_Eklipse - Convert Twitch Clip To TikTok, Reels,  Shorts For Free/Page_Eklipse studio/p_Small Facecam'))

WebUI.click(findTestObject('Page_Eklipse - Convert Twitch Clip To TikTok, Reels,  Shorts For Free/Page_Eklipse studio/button_Continue Editing'))

WebUI.click(findTestObject('Page_Eklipse - Convert Twitch Clip To TikTok, Reels,  Shorts For Free/Page_Eklipse studio/button_Next_face'))

WebUI.click(findTestObject('Page_Eklipse - Convert Twitch Clip To TikTok, Reels,  Shorts For Free/Page_Eklipse studio/button_Next_feed'))

WebUI.click(findTestObject('Page_Eklipse - Convert Twitch Clip To TikTok, Reels,  Shorts For Free/Page_Eklipse studio/button_Confirm'))

WebUI.click(findTestObject('Page_Eklipse - Convert Twitch Clip To TikTok, Reels,  Shorts For Free/Page_Eklipse studio/button_Got it'))

