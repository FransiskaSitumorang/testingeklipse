<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_OR_password_confirmation</name>
   <tag></tag>
   <elementGuidId>383bfcc3-ae6e-4765-bac3-7be969ec4af7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#password_confirmation</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='password_confirmation']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>edde07f4-cdbd-4975-959e-fd06c78c7fea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password_confirmation</value>
      <webElementGuid>f52c281a-8752-4495-99a2-156e76e23af9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>e4521419-94f2-486e-aed3-a636bdbc9ccd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>65696267-68e0-46ba-adf8-5d9adfdf0dbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password_confirmation</value>
      <webElementGuid>9e72654a-7dd3-46e8-9a58-45cb79b84861</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Confirm password</value>
      <webElementGuid>69bcae82-30db-48b2-babf-e147b7a41fc0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>8b0d24f1-f9f9-40cf-828a-07fc4386c699</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password_confirmation&quot;)</value>
      <webElementGuid>1541dfae-b03f-462e-97e5-3f53e10c11d5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='password_confirmation']</value>
      <webElementGuid>3391e74e-9798-439c-89b5-1ed1e281563c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div/div/div/form/div[4]/input</value>
      <webElementGuid>bfd3e0be-8d48-4c81-bdc6-361d22e1f5a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/input</value>
      <webElementGuid>835f34de-c21e-4dd7-9441-6c2f13a33452</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'password_confirmation' and @type = 'password' and @name = 'password_confirmation' and @placeholder = 'Confirm password']</value>
      <webElementGuid>1c01db49-294b-4592-8c1d-337c4a9bc31b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
