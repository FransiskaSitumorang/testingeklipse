<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_use the link</name>
   <tag></tag>
   <elementGuidId>9d467dbb-7ffa-4224-a186-feb7e573f7a5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.MuiTypography-root.MuiTypography-body1.css-1oyoqcg-MuiTypography-root</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/div[2]/div/div/p/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2852f203-43ca-4462-be72-ceffa63feada</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 css-1oyoqcg-MuiTypography-root</value>
      <webElementGuid>d7df82ab-491d-42d0-804d-b02a659d092a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>use the link</value>
      <webElementGuid>e0bc1130-695c-4cf6-a324-247dcad1db5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiBox-root css-113yq0e&quot;]/main[@class=&quot;MuiBox-root css-1o39im8&quot;]/div[@class=&quot;MuiBox-root css-1p0vhvo&quot;]/div[@class=&quot;MuiBox-root css-rub0yr&quot;]/div[@class=&quot;MuiBox-root css-1swqya0&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body1 css-1esffwj-MuiTypography-root&quot;]/button[@class=&quot;MuiTypography-root MuiTypography-body1 css-1oyoqcg-MuiTypography-root&quot;]</value>
      <webElementGuid>6872ff64-9d81-48ba-991b-cc0edfc0ae0f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/div[2]/div/div/p/button</value>
      <webElementGuid>73d55a41-66b6-4953-9c94-411149d7fe60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Upload Clip'])[2]/following::button[1]</value>
      <webElementGuid>55ff80be-1efa-4b45-8c11-1342051dd0fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Clip'])[1]/following::button[2]</value>
      <webElementGuid>6a1afde3-8ed7-4f27-9dad-be97e037fc7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Continue Editing'])[1]/preceding::button[1]</value>
      <webElementGuid>534fc389-22ee-42aa-8cd6-2018c9cfa4a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic'])[1]/preceding::button[2]</value>
      <webElementGuid>dfdc1599-db22-4397-9502-711b3cb4116f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='use the link']/parent::*</value>
      <webElementGuid>2ff4cb6a-24a3-426a-8eff-c19e1a7e1101</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p/button</value>
      <webElementGuid>d6763325-e7ad-482a-bc3f-ac21e37cf7a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'use the link' or . = 'use the link')]</value>
      <webElementGuid>44fe24bc-e3cc-402b-aaaa-e892e585c852</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
