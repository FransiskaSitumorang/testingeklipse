<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Next_face</name>
   <tag></tag>
   <elementGuidId>49703e1d-88dc-4952-98ef-ceb6176a4126</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.MuiButton-root.MuiButton-contained.MuiButton-containedEklipse.MuiButton-sizeLarge.MuiButton-containedSizeLarge.MuiButtonBase-root.css-44xs8h-MuiButtonBase-root-MuiButton-root</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[5]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7c62d0c9-f8ed-4bf3-971a-c3c1d19e0b9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiButton-root MuiButton-contained MuiButton-containedEklipse MuiButton-sizeLarge MuiButton-containedSizeLarge MuiButtonBase-root css-44xs8h-MuiButtonBase-root-MuiButton-root</value>
      <webElementGuid>4103615c-b4a9-42f7-98e2-dd0a79039c28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>297a0e87-eead-4103-a240-942a4f5f65fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4f235711-a666-4c3e-9df1-e72da421ba7f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Next</value>
      <webElementGuid>9a530563-c19c-4d64-a885-3c4f2bb0a08b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiBox-root css-113yq0e&quot;]/main[@class=&quot;MuiBox-root css-1o39im8&quot;]/div[@class=&quot;MuiBox-root css-gg4vpm&quot;]/button[@class=&quot;MuiButton-root MuiButton-contained MuiButton-containedEklipse MuiButton-sizeLarge MuiButton-containedSizeLarge MuiButtonBase-root css-44xs8h-MuiButtonBase-root-MuiButton-root&quot;]</value>
      <webElementGuid>ded4d884-3b12-4b27-9ac5-918be57142b2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[5]</value>
      <webElementGuid>20af80e8-126f-48b1-b4f5-bf1f881c9f8d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/div[2]/button[2]</value>
      <webElementGuid>1dc57e6e-894a-4aa7-ad70-0a861982219b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/following::button[1]</value>
      <webElementGuid>c01a0ff8-875e-409c-92de-fd7c47561320</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select Your Game Feed Area'])[1]/following::button[2]</value>
      <webElementGuid>a14bef35-b583-4df5-8969-e422796d276a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::button[1]</value>
      <webElementGuid>9fc0b289-c387-4457-952c-5d99888f1bba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::button[1]</value>
      <webElementGuid>dee5567c-08af-4e9b-82ce-7caac6eb725c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Next']/parent::*</value>
      <webElementGuid>6dfbd40a-d9a7-4c49-b812-ed6114af912e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button[2]</value>
      <webElementGuid>d9c91777-42ca-47ff-b3c2-e40c6b092b03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Next' or . = 'Next')]</value>
      <webElementGuid>ab14fab8-e0cb-4af7-b2bd-0e3fdbec9ea3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
