<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img</name>
   <tag></tag>
   <elementGuidId>1d6f850b-fafe-485c-a8e4-b269f7d43691</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-img.MuiBox-root.css-0 > img</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='template-1']/div/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>00119bdb-afff-45db-ad1b-e9bf2f100875</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/temp_smallFacecam.webp</value>
      <webElementGuid>86e0fa30-7b73-41bc-a459-4aa045e75e58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;template-1&quot;)/div[@class=&quot;card-img MuiBox-root css-0&quot;]/img[1]</value>
      <webElementGuid>03b4ea90-2d4f-4fe5-bb52-965dd3c886ae</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='template-1']/div/img</value>
      <webElementGuid>06088cb2-06dc-4a7a-90c9-dfdf7be2c61f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/img</value>
      <webElementGuid>3958118a-f115-438f-ba96-752bc9119e27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = '/temp_smallFacecam.webp']</value>
      <webElementGuid>75480b19-ef3c-4d16-9bc1-64d4389d704a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
