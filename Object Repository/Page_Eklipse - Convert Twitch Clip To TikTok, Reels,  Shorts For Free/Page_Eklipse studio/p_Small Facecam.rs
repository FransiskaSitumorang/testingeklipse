<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Small Facecam</name>
   <tag></tag>
   <elementGuidId>7633b781-a57b-43dd-9734-02be2b52eb2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.MuiTypography-root.MuiTypography-body1.title.css-1esffwj-MuiTypography-root</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='template-1']/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>c510cb78-e03f-4488-930a-73e439e08ec0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 title css-1esffwj-MuiTypography-root</value>
      <webElementGuid>10a436ec-f081-4930-9ec0-ea7a228288f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Small Facecam</value>
      <webElementGuid>d648dfe9-ceba-45ed-a881-80470273e072</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;template-1&quot;)/p[@class=&quot;MuiTypography-root MuiTypography-body1 title css-1esffwj-MuiTypography-root&quot;]</value>
      <webElementGuid>eb351e52-cc85-4325-b5fb-07f07fca1b2f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='template-1']/p</value>
      <webElementGuid>78a32430-4e7b-45eb-b4d7-610be9b9c1ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Free'])[1]/following::p[1]</value>
      <webElementGuid>8f666a84-ed19-43bf-b521-6c6b46700034</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MWII'])[1]/following::p[1]</value>
      <webElementGuid>20527fe8-1714-4b7b-8aa4-349892428828</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Free'])[2]/preceding::p[1]</value>
      <webElementGuid>b86acec2-80b2-4432-8bab-a430f3cb4b37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Free'])[3]/preceding::p[2]</value>
      <webElementGuid>170fa0b4-6316-4686-acc4-156f6427dae0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Small Facecam']/parent::*</value>
      <webElementGuid>7a68331a-2895-4286-8cc6-a8aae34b13ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/p</value>
      <webElementGuid>3d3c51e4-0e4b-4ec3-b056-9d0a900ac5f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Small Facecam' or . = 'Small Facecam')]</value>
      <webElementGuid>d148d4fe-cb74-4ae0-98f1-2d9b37608cb8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
