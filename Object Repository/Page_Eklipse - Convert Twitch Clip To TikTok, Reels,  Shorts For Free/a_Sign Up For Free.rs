<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign Up For Free</name>
   <tag></tag>
   <elementGuidId>13649579-f2c2-482a-a67f-875b6cea8136</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.header-main__col.header-main__col--right > a.btn.btn-register</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'Sign Up For Free')])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>27111374-4946-4f35-8723-fe3a45539eef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://app.eklipse.gg/register?_gl=1*1bo13tp*_ga*MTM1ODI2MDgxNi4xNjc3MjkwNDE0*_ga_QWLNXXMD6H*MTY3NzI5MDQxMy4xLjAuMTY3NzI5MDQxMy4wLjAuMA..*_ga_GLD7CWERS9*MTY3NzI5MDQyMS4xLjAuMTY3NzI5MDQyMS4wLjAuMA..*_ga_WQX826KJ2T*MTY3NzI5MDQyMy4xLjAuMTY3NzI5MDQyMy4wLjAuMA..&amp;_ga=2.29895163.558088931.1677290416-1358260816.1677290414</value>
      <webElementGuid>695441ae-291d-4b70-8286-83069a870b55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-register</value>
      <webElementGuid>01af0103-c915-42eb-8ddf-7bc79551fe76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						Sign Up For Free
					</value>
      <webElementGuid>eaddec5e-085c-4915-97ac-e3081253f096</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-js mysticky-welcomebar-apper&quot;]/body[@class=&quot;home page-template-default page page-id-2768 wp-custom-logo e-lazyload elementor-default elementor-kit-9 elementor-page elementor-page-2768 e--ua-blink e--ua-chrome e--ua-mac e--ua-webkit mysticky-welcomebar-apper&quot;]/header[@class=&quot;header&quot;]/div[@class=&quot;header-main&quot;]/div[@class=&quot;header-main__container&quot;]/div[@class=&quot;header-main__col header-main__col--right&quot;]/a[@class=&quot;btn btn-register&quot;]</value>
      <webElementGuid>96f90265-3110-42a6-8584-57adb72417ac</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Sign Up For Free')])[2]</value>
      <webElementGuid>8f9d603c-b459-4b8c-9589-1c047427a127</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign In'])[2]/following::a[1]</value>
      <webElementGuid>69e8c9ea-e463-4e61-8dff-0c8fcd6a3f45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Up For Free'])[1]/following::a[2]</value>
      <webElementGuid>b37d3656-e953-4d46-96bb-6fc9f99a1ad4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The Best of'])[1]/preceding::a[1]</value>
      <webElementGuid>6bd39989-8df5-4d92-a728-5ea8337ea5fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://app.eklipse.gg/register?_gl=1*1bo13tp*_ga*MTM1ODI2MDgxNi4xNjc3MjkwNDE0*_ga_QWLNXXMD6H*MTY3NzI5MDQxMy4xLjAuMTY3NzI5MDQxMy4wLjAuMA..*_ga_GLD7CWERS9*MTY3NzI5MDQyMS4xLjAuMTY3NzI5MDQyMS4wLjAuMA..*_ga_WQX826KJ2T*MTY3NzI5MDQyMy4xLjAuMTY3NzI5MDQyMy4wLjAuMA..&amp;_ga=2.29895163.558088931.1677290416-1358260816.1677290414')]</value>
      <webElementGuid>27f75098-4773-4f56-98ec-57bae3dc15da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a[2]</value>
      <webElementGuid>d33d5bcd-a376-4351-a716-695c91a31a35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://app.eklipse.gg/register?_gl=1*1bo13tp*_ga*MTM1ODI2MDgxNi4xNjc3MjkwNDE0*_ga_QWLNXXMD6H*MTY3NzI5MDQxMy4xLjAuMTY3NzI5MDQxMy4wLjAuMA..*_ga_GLD7CWERS9*MTY3NzI5MDQyMS4xLjAuMTY3NzI5MDQyMS4wLjAuMA..*_ga_WQX826KJ2T*MTY3NzI5MDQyMy4xLjAuMTY3NzI5MDQyMy4wLjAuMA..&amp;_ga=2.29895163.558088931.1677290416-1358260816.1677290414' and (text() = '
						Sign Up For Free
					' or . = '
						Sign Up For Free
					')]</value>
      <webElementGuid>3060d14d-d8e0-4a2c-ae9c-d5903e748b1b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
