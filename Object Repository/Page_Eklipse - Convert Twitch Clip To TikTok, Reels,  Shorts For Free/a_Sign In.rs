<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign In</name>
   <tag></tag>
   <elementGuidId>7374e1e4-acee-4daa-bc67-362a8088f86c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.header-main__col.header-main__col--right > a.btn.btn-login</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[contains(text(),'Sign In')])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7f02603c-78da-4741-abe0-57f778f9fb8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://app.eklipse.gg/login?_gl=1*bjmxl0*_ga*MTc2MDQ0NTUwMi4xNjc3MzA1MTQ0*_ga_QWLNXXMD6H*MTY3NzMwNTE0NC4xLjAuMTY3NzMwNTE0NC4wLjAuMA..*_ga_WQX826KJ2T*MTY3NzMwNTE2Ni4xLjEuMTY3NzMwNTE2Ni4wLjAuMA..*_ga_GLD7CWERS9*MTY3NzMwNTE2Ni4xLjAuMTY3NzMwNTE2Ni4wLjAuMA..&amp;_ga=2.142698044.844272903.1677305161-1760445502.1677305144</value>
      <webElementGuid>d42883d6-952f-4641-9d5b-272db15b9ac1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-login</value>
      <webElementGuid>da8b4b6c-d38d-4221-b137-77eb6b959ad5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
						Sign In
					</value>
      <webElementGuid>22939ac7-d540-4553-85b8-cc8e2d90807c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-js mysticky-welcomebar-apper&quot;]/body[@class=&quot;home page-template-default page page-id-2768 wp-custom-logo e-lazyload elementor-default elementor-kit-9 elementor-page elementor-page-2768 e--ua-blink e--ua-chrome e--ua-mac e--ua-webkit mysticky-welcomebar-apper&quot;]/header[@class=&quot;header&quot;]/div[@class=&quot;header-main&quot;]/div[@class=&quot;header-main__container&quot;]/div[@class=&quot;header-main__col header-main__col--right&quot;]/a[@class=&quot;btn btn-login&quot;]</value>
      <webElementGuid>fbee1410-caf1-4b9a-9d3a-2db09de24afe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Sign In')])[2]</value>
      <webElementGuid>341c3969-91bd-45cc-963a-814017902637</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Up For Free'])[1]/following::a[1]</value>
      <webElementGuid>fddca4bc-9075-495c-a649-b04515548ddb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign In'])[1]/following::a[2]</value>
      <webElementGuid>fe9bdad7-3943-4601-a163-e54fb64954f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Up For Free'])[2]/preceding::a[1]</value>
      <webElementGuid>6f24752b-70c6-4e66-bc84-e4f2069a7d44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://app.eklipse.gg/login?_gl=1*bjmxl0*_ga*MTc2MDQ0NTUwMi4xNjc3MzA1MTQ0*_ga_QWLNXXMD6H*MTY3NzMwNTE0NC4xLjAuMTY3NzMwNTE0NC4wLjAuMA..*_ga_WQX826KJ2T*MTY3NzMwNTE2Ni4xLjEuMTY3NzMwNTE2Ni4wLjAuMA..*_ga_GLD7CWERS9*MTY3NzMwNTE2Ni4xLjAuMTY3NzMwNTE2Ni4wLjAuMA..&amp;_ga=2.142698044.844272903.1677305161-1760445502.1677305144')]</value>
      <webElementGuid>39a15a73-071c-475a-971c-a007ae082228</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a</value>
      <webElementGuid>5c4cfda4-4582-4455-9366-f88f561ec8e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://app.eklipse.gg/login?_gl=1*bjmxl0*_ga*MTc2MDQ0NTUwMi4xNjc3MzA1MTQ0*_ga_QWLNXXMD6H*MTY3NzMwNTE0NC4xLjAuMTY3NzMwNTE0NC4wLjAuMA..*_ga_WQX826KJ2T*MTY3NzMwNTE2Ni4xLjEuMTY3NzMwNTE2Ni4wLjAuMA..*_ga_GLD7CWERS9*MTY3NzMwNTE2Ni4xLjAuMTY3NzMwNTE2Ni4wLjAuMA..&amp;_ga=2.142698044.844272903.1677305161-1760445502.1677305144' and (text() = '
						Sign In
					' or . = '
						Sign In
					')]</value>
      <webElementGuid>ebc9a12d-118d-4d89-8036-ce11ed60e550</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
