<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Log in</name>
   <tag></tag>
   <elementGuidId>616ccd8d-b6dc-471d-82b1-0b96a94856cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.e1w6iovg0.tiktok-1vrn65e-Button-StyledButton.ehk74z00</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>918a2cfc-022e-49ad-b5e6-7e170ba2b673</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>9f908303-6018-4741-baa5-1913682f8153</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-e2e</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>9a56111b-382a-44c4-9025-bf441e7243b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>e1w6iovg0 tiktok-1vrn65e-Button-StyledButton ehk74z00</value>
      <webElementGuid>12d0311a-48cb-4e75-8999-c05a65136975</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Log in</value>
      <webElementGuid>c113c137-d9e8-409f-8cd5-41783e007bb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loginContainer&quot;)/div[@class=&quot;tiktok-xabtqf-DivLoginContainer exd0a430&quot;]/form[1]/button[@class=&quot;e1w6iovg0 tiktok-1vrn65e-Button-StyledButton ehk74z00&quot;]</value>
      <webElementGuid>ae0e4464-e596-46a8-937c-52c8469159d1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='submit']</value>
      <webElementGuid>6ad49ff3-a529-4f1a-93c8-206bc650cd17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='loginContainer']/div/form/button</value>
      <webElementGuid>52ff0db5-6e2c-4e11-8851-dcd6e625ce5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot password?'])[1]/following::button[1]</value>
      <webElementGuid>cbce119f-c4eb-4c73-89e2-a3ddaa98cd1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log in with phone'])[1]/following::button[1]</value>
      <webElementGuid>8e9a5cc0-e82e-494f-8d45-34cd1379f9e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go back'])[1]/preceding::button[1]</value>
      <webElementGuid>a6d0a0e9-a620-428a-a647-ea2549e84551</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Don’t have an account?'])[1]/preceding::button[1]</value>
      <webElementGuid>6c3bb80a-eb9f-4801-8cf8-4d4e59cd35e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>9ca51c4b-c6f3-4c65-858c-56e1fa64520c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and (text() = 'Log in' or . = 'Log in')]</value>
      <webElementGuid>1b2bf28c-3236-4aa3-9368-ff9224e5307f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
